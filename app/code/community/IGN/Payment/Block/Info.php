<?php

/**
 * Base payment iformation block
 *
 */
class IGN_Payment_Block_Info extends Mage_Payment_Block_Info
{


    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('ignpayment/info.phtml');
    }

}
