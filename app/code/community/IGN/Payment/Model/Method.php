<?php
class IGN_Payment_Model_Method extends Mage_Payment_Model_Method_Abstract {

    protected $_code = 'ignpayment';

    protected $_formBlockType = 'ignpayment/form';
    protected $_infoBlockType = 'ignpayment/info';


    public function validate()
    {
        $code = Mage::app()->getRequest()->getParam('secret_code');
        if($code != $this->getConfigData('secret_code')) {
            Mage::throwException(Mage::helper('ignpayment')->__("This code doesn't work!"));
        }
        return parent::validate();
    }
}